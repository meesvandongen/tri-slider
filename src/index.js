import React, { Component } from 'react';
import t from 'prop-types';
import almostEqual from 'almost-equal';

const cos30 = 0.86602540378;
const heightFromTriangleWidth = size => size * cos30;
const lengthFromTrianglePoint = ({ x, y }) => x * cos30 + y * 0.5;
const snappingPoints = [
  [0.3333333333, 0.3333333333, 0.3333333333],
  [0.5, 0.5, 0.0],
  [0.0, 0.5, 0.5],
  [0.5, 0.0, 0.5],
];

const snapAt = (values, snappingPoint) => {
  if (
    almostEqual(values[0], snappingPoint[0], 0.02) &&
    almostEqual(values[1], snappingPoint[1], 0.02) &&
    almostEqual(values[2], snappingPoint[2], 0.02)
  ) {
    return [snappingPoint[0], snappingPoint[1], snappingPoint[2]];
  }
  return values;
};

const snappedValues = values => {
  snappingPoints.forEach(snappingPoint => {
    values = snapAt(values, snappingPoint);
  });
  return values;
};

const clamp = (val, min, max) => {
  if (val < min) return min;
  else if (val > max) return max;
  return val;
};

const widthForCurrentHeight = (maxWidth, currentHeight) =>
  clamp(
    maxWidth * (currentHeight / heightFromTriangleWidth(maxWidth)),
    0,
    maxWidth,
  );

const clampTriangle = ({ x, y }, width) => {
  const horizontalSlice = widthForCurrentHeight(width, y);
  const height = heightFromTriangleWidth(width);
  return {
    x: clamp(
      x,
      (width - horizontalSlice) / 2,
      (width - horizontalSlice) / 2 + horizontalSlice,
    ),
    y: clamp(y, 0, height),
  };
};

export default class TriSlider extends Component {
  static propTypes = {
    knob: t.node,
    size: t.number,
    backgroundColor: t.string,
    style: t.object,
    isThreeWay: t.bool,
    onChange: t.func.isRequired,
    value: t.array,
    labelA: t.func,
    labelB: t.func,
    labelC: t.func,
  };
  static defaultProps = {
    knob: (
      <div
        style={{
          width: 20,
          height: 20,
          background: '#ccc',
          borderRadius: '50%',
        }}
      />
    ),
    size: 400,
    backgroundColor: `
      radial-gradient(circle at top, #ff0000, transparent),
      radial-gradient(circle at left bottom, #00ff00, transparent),
      radial-gradient(circle at right bottom, #0000ff, transparent)
    `,
    style: {},
    isThreeWay: true,
    value: undefined,
    labelA: val => val,
    labelB: val => val,
    labelC: val => val,
  };
  constructor() {
    super();
    this.slider = React.createRef();
  }
  state = {
    ratios: [0.33, 0.33, 0.33],
  };

  onMouseDown = e => {
    this.onMouseMove(e);
    document.addEventListener('mouseup', this.onMouseUp);
    document.addEventListener('mousemove', this.onMouseMove);
    e.preventDefault();
  };
  onMouseUp = e => {
    document.removeEventListener('mouseup', this.onMouseUp);
    document.removeEventListener('mousemove', this.onMouseMove);
    e.preventDefault();
  };
  onMouseMove = e => {
    let ratios;
    const currentRatios = this.getCurrentRatios();
    const nextCoords = this.relativeCoords(e);
    if (this.props.isThreeWay) {
      const clampedRelativeCoords = clampTriangle(nextCoords, this.props.size);
      ratios = this.ratiosFromCoords(clampedRelativeCoords);
    } else {
      const nextHorizontalPosition = clamp(
        nextCoords.x / this.props.size,
        0,
        1,
      );
      if (nextHorizontalPosition < 0.5) {
        const R = nextHorizontalPosition / 0.5;
        if (currentRatios[2] === 0) {
          const A = 1 / (1 + R);
          const B = 1 - A;
          const C = 0;
          ratios = [A, B, C];
        } else {
          const N =
            Math.max(currentRatios[0], currentRatios[1]) / currentRatios[2];
          const C = 1 / (R * N + N + 1);
          const A = N * C;
          const B = R * A;
          ratios = [A, B, C];
        }
      } else {
        // B is greater than A
        const R = (1 - nextHorizontalPosition) / 0.5;
        if (currentRatios[2] === 0) {
          const B = 1 / (1 + R);
          const A = 1 - B;
          const C = 0;
          ratios = [A, B, C];
        } else {
          const N =
            Math.max(currentRatios[0], currentRatios[1]) / currentRatios[2];
          const C = 1 / (R * N + N + 1);
          const B = N * C;
          const A = R * B;
          ratios = [A, B, C];
        }
      }
    }
    ratios = snappedValues(ratios);
    this.props.onChange(ratios);
    if (!this.isControlled()) {
      this.setState({
        ratios,
      });
    }

    e.preventDefault();
  };

  getCurrentRatios = () => {
    if (this.isControlled()) {
      return this.props.value;
    }
    return this.state.ratios;
  };

  ratiosFromCoords = ({ x, y }) => {
    const height = heightFromTriangleWidth(this.props.size);
    return [
      1 -
        lengthFromTrianglePoint({
          x,
          y: -y + height,
        }) /
          height,
      1 -
        lengthFromTrianglePoint({
          x: this.props.size - x,
          y: y * -1 + height,
        }) /
          height,
      1 - y / height,
    ];
  };

  coordsFromRatios = ratios => {
    const height = heightFromTriangleWidth(this.props.size);
    if (this.props.isThreeWay) {
      const y = height - (1 - ratios[2]) * height;
      const x = ((1 - ratios[0]) * height - y * 0.5) / cos30;
      return { x, y };
    }
    const [A, B, C] = ratios;
    if (A > B) {
      if (C === 0) {
        return {
          x: ((1 - A) / A / 2) * this.props.size,
          y: '50%',
        };
      }
      return {
        x: (((1 / C - 1) / (A / C) - 1) / 2) * this.props.size,
        y: '50%',
      };
    }
    if (C === 0) {
      return {
        x: (1 - (1 / B - 1) / 2) * this.props.size,
        y: '50%',
      };
    }
    return {
      /**
        R/2 = 1 - I
        I = 1 - (R/2)

        N*R + N + 1 = 1 / C
        N * R + N = 1/ C - 1
        R + 1 = (1/C - 1) / N
        R = (1/C - 1) / N - 1

        N = B / C
       */
      x: (1 - ((1 / C - 1) / (B / C) - 1) / 2) * this.props.size,
      y: '50%',
    };
  };

  isControlled = () => this.props.value !== undefined;

  relativeCoords = event => {
    const bounds = this.slider.current.getBoundingClientRect();
    const x = event.clientX - bounds.left;
    const y = event.clientY - bounds.top;
    return { x, y };
  };

  renderableCoords = () => {
    const currentRatios = this.getCurrentRatios();
    const currentCoords = this.coordsFromRatios(currentRatios);
    if (this.props.isThreeWay) {
      return {
        x: currentCoords.x,
        y: `${(currentCoords.y / heightFromTriangleWidth(this.props.size)) *
          100}%`,
      };
    }
    return this.coordsFromRatios(currentRatios);
  };

  render = () => {
    const { size, style, knob, isThreeWay, backgroundColor } = this.props;
    const renderableCoords = this.renderableCoords();
    const getAValue = () => {
      if (!this.props.isThreeWay) {
        return 1 - renderableCoords.x / this.props.size;
      }
      return this.getCurrentRatios()[0];
    };
    const getBValue = () => {
      if (!this.props.isThreeWay) {
        return renderableCoords.x / this.props.size;
      }
      return this.getCurrentRatios()[1];
    };
    return (
      <div
        style={{
          width: size,
          margin: 10,
        }}
      >
        <div
          style={{
            textAlign: 'center',
            marginBottom: 10,
            display: isThreeWay ? 'block' : 'none',
          }}
        >
          {this.props.labelC(this.getCurrentRatios()[2])}
        </div>
        <div style={{ position: 'relative' }}>
          <div
            style={{
              width: size,
              height: isThreeWay ? heightFromTriangleWidth(size) : 6,
              clipPath: isThreeWay
                ? 'polygon(50% 0, 50% 0, 100% 100%, 0% 100%)'
                : 'polygon(0 0, 100% 0, 100% 100%, 0% 100%)',
              background: backgroundColor,
              // `
              // radial-gradient(circle at 50% 66.66%, white, black 80%),
              // conic-gradient(at 50% 66.6666% , purple, lime, blue, purple) // Conic gradients are not supported yet :(
              // `,
              backgroundBlendMode: 'screen',
              transition: 'all 0.2s ease, width 0.2s ease',
              ...style,
            }}
            role='grid'
            tabIndex={0}
            ref={this.slider}
            onMouseDown={this.onMouseDown}
          />
          <div
            style={{
              position: 'absolute',
              left: renderableCoords.x,
              bottom: renderableCoords.y,
              transform: 'translate(-50%, 50%)',
              pointerEvents: 'none',
            }}
          >
            {knob}
          </div>
        </div>
        <div
          style={{
            marginTop: 10,
          }}
        >
          <span>{this.props.labelA(getAValue())}</span>
          <span style={{ float: 'right' }}>
            {this.props.labelB(getBValue())}
          </span>
        </div>
      </div>
    );
  };
}
