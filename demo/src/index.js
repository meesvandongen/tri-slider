import React, { Component } from 'react';
import { render } from 'react-dom';

import Slider from '../../src';

class Demo extends Component {
  state = {
    threeWay: true,
    value: [0.33, 0.33, 0.33],
  };
  render() {
    return (
      <div>
        <h1>tri-slider Demo</h1>
        <button
          onClick={() => this.setState({ threeWay: !this.state.threeWay })}
        >
          fds
        </button>
        <Slider
          isThreeWay={this.state.threeWay}
          onChange={e => this.setState({ value: e })}
          value={this.state.value}
          labelA={valueA => `${+(valueA * 100).toFixed()}%`}
          labelB={valueA => `${+(valueA * 100).toFixed()}%`}
          labelC={valueA => `${+(valueA * 100).toFixed()}%`}
        />
      </div>
    );
  }
}

render(<Demo />, document.querySelector('#demo'));
